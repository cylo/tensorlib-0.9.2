# Uni10 #

Welcome to Uni10, the Universal Tensor Network Library.

Uni10 is programmed in C++ and geared toward applications to the tensor
network algorithms, such TEBD, PEPS and MERA. We hope the availability
of Uni10 will make future implementation of tensor network algorithms much
easier and less error prone.

Developers:

* Yun-Da Hsieh (National Taiwan University)
* Ying-Jer Kao (National Taiwan University)
* Pochung Chen (National Tsing-Hua University)
* Tama Ma (Singapore National University)
* Sukhbinder Singh (Macquarie University)

